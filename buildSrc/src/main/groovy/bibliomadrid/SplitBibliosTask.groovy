package bibliomadrid
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputFile
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction

class SplitBibliosTask extends DefaultTask{

    @InputFile
    File getCsv(){
        project.file("$project.buildDir/bibliotecas.csv")
    }

    @Input
    int linesPerFile = 30000

    @Optional
    @Input
    String separator = ';'

    @OutputDirectory
    File getSplitDirectory(){
        project.file("$project.buildDir.name/biblios")
    }

    @Internal
    int countFile=0

    @Internal
    File getCurrentSplitFile(){
        project.file(
                "$splitDirectory.absolutePath/bibliotecas_${String.format('%02d', countFile)}.csv")
    }

    @TaskAction
    void main(){

        List<String>header
        csv.withReader {
            String line
            while( (line= it.readLine()).trim().length() == 0){}
            header = line.split(';').collect{ it.replaceAll('-','_') } as List<String>
        }

        countFile=0
        int countLine = -1
        List<List<String>> lines = []
        csv.eachLine { line ->
            if( line.trim().length() == 0)
                return
            if( ++countLine == 0)
                return
            line = line.replaceAll('\\|',' ')

            String code = line.split(';').first()
            line = line.split(';').drop(1).join(';')
            def match = line =~ /"([^"]*)"/

            List<String> fields = [code]+match.collect{ it[1].replaceAll(';',' ').trim() } as List<String>
            if( fields.size() == header.size()){
                lines.add fields
            }
            if( (lines.size() % linesPerFile) == 0 ){
                dumpLines(header, lines).clear()
            }
        }
        dumpLines(header, lines).clear()
    }

    List<List<String>> dumpLines(List<String> header, List<List<String>> lines){
        File current = currentSplitFile
        current.withWriter { w ->
            w.println header.join('|')
            lines.each{
                w.println it.join('|')
            }
        }
        countFile++
        lines
    }

}

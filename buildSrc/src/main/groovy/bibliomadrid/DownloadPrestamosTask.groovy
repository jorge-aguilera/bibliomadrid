package bibliomadrid

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction

class DownloadPrestamosTask extends DefaultTask{

    @Input
    int year

    @Input
    int month

    @OutputFile
    File getCsv(){
        project.file("build/bibliotecas_prestamos_${String.format('%04d', year)}${String.format('%02d', month)}.csv")
    }

    @Internal
    String baseUrl = 'https://datos.madrid.es:443/datosabiertos/CULTURA/BIBLIOTECAS_PRESTAMOS/%04d/%02d/bibliotecas_prestamos_%04d%02d.csv'

    @TaskAction
    void main(){
        try {
            csv.withWriter { w ->
                w << new InputStreamReader(new URL(String.format(baseUrl, year, month, year, month)).openStream(), 'iso-8859-1')
            }
        }catch(e){
            println e.message
        }
    }

}

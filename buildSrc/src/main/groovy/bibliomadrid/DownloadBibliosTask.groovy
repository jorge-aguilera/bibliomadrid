package bibliomadrid

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction

class DownloadBibliosTask extends DefaultTask{

    @OutputFile
    File getCsv(){
        project.file("build/bibliotecas.csv")
    }

    @Internal
    String baseUrl = 'https://datos.madrid.es/portal/site/egob/menuitem.ac61933d6ee3c31cae77ae7784f1a5a0/?vgnextoid=00149033f2201410VgnVCM100000171f5a0aRCRD&format=csv&file=0&filename=201747-0-bibliobuses-bibliotecas&charset=ISO-8859-1&mgmtid=ed35401429b83410VgnVCM1000000b205a0aRCRD&preview=full'

    @TaskAction
    void main(){
        csv.withWriter{ w->
            w << new InputStreamReader( new URL(baseUrl).openStream(),'iso-8859-1')
        }
    }
}

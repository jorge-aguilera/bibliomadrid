package bibliomadrid

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputFile
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction

class SplitPrestamosTask extends DefaultTask{

    @Input
    int year

    @Input
    int month

    @InputFile
    File getCsv(){
        project.file("$project.buildDir/bibliotecas_prestamos_${String.format('%04d', year)}${String.format('%02d', month)}.csv")
    }

    @Input
    int linesPerFile = 30000

    @Optional
    @Input
    String separator = ';'

    @OutputDirectory
    File getSplitDirectory(){
        project.file("$project.buildDir.name/split")
    }

    @Internal
    int countFile=0

    @Internal
    File getCurrentSplitFile(){
        project.file(
                "$splitDirectory.absolutePath/bibliotecas_prestamos_${String.format('%04d', year)}${String.format('%02d', month)}_${String.format('%02d', countFile)}.csv")
    }

    @TaskAction
    void main(){
        List<String>header
        csv.withReader {
            String line = it.readLine()
            def match = line =~ /"([^"]*)"/
            header = match.collect{ it[1].trim() } as List<String>
        }
        if(header.size() == 11) header.add('tiisxn')

        countFile=0
        int countLine = -1
        List<List<String>> lines = []
        csv.eachLine { line ->
            if( ++countLine == 0)
                return
            line = line.replaceAll('\\|',' ')

            def match = line =~ /"([^"]*)"/
            List<String> fields = match.collect{ it[1].replaceAll(';',' ').trim() } as List<String>
            if(fields.size() == 11) fields.add('n/a')

            if( fields.size() == header.size()){
                //yyyy-MM-dd
                fields[7] = '20'+fields[7][6..7] +'-'+ fields[7][3..4] +'-'+ fields[7][0..1]
                lines.add fields
            }
            if( (lines.size() % linesPerFile) == 0 ){
                dumpLines(header, lines).clear()
            }
        }
        dumpLines(header, lines).clear()
    }

    List<List<String>> dumpLines(List<String> header, List<List<String>> lines){
        File current = currentSplitFile
        current.withWriter { w ->
            w.println header.join('|')
            lines.each{
                w.println it.join('|')
            }
        }
        countFile++
        lines
    }

}

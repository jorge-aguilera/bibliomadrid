
@Grapes(
        @Grab(group='org.apache.pdfbox', module='pdfbox', version='2.0.8')
)
import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.text.*
import java.awt.Rectangle
//end::dependencies[]


println new PDFTextStripper().getText(PDDocument.load(new URL(args[0]).bytes))
@Grab('com.puravida.groogle:groogle-core:2.1.1')
@Grab('com.puravida.groogle:groogle-bigquery:0.1.0-milestone.0.1+20190615T094700Z')

import com.puravida.groogle.*
import com.puravida.groogle.bigquery.*

groogle = GroogleBuilder.build {
    withServiceCredentials {
    }
    service(BigQueryBuilder.build(), BigQueryService)
}

service = groogle.service(BigQueryService)

void total(params) {
    println "Total prestamos " +
            service.rows("SELECT COUNT(*) prestamos FROM `m30-bot.test.prestamos` ").first().prestamos
}

void topLibros(params) {
    println "Top libros "
    service.eachRow """SELECT tititu, count(*) prestamos from `m30-bot.test.prestamos`
    where prcocp='PRL'
    group by 1 order by 2 desc limit 50
""", {
        println "$it.tititu , prestado $it.prestamos"
    }
}

void topHaceMeses(params) {
    Calendar c = Calendar.instance
    c.add(Calendar.MONTH, -1*(params.first() as int))

    println "Top libros ${c[Calendar.MONTH]}/${c[Calendar.YEAR]}"

    service.eachRow """SELECT tititu, count(*) prestamos from `m30-bot.test.prestamos`
    WHERE prcocp in ('PRL','PRLN')
    and EXTRACT(month from prfpre) = ${c[Calendar.MONTH]} 
    and EXTRACT(year from prfpre)=${c[Calendar.YEAR]}
    group by 1 order by 2 desc limit 10
""", {
        println "\t $it.tititu , prestado $it.prestamos"
    }
}

this."${args[0]}"(args.drop(1))